﻿using System;
using Rage.Native;

namespace Rage.UI
{
	/// <summary>
	/// Methods to manipulate the HUD (heads-up-display) of the game.
	/// </summary>
	public static class Hud
	{
		/// <summary>
		/// Determines whether the specified component is active.
		/// </summary>
		/// <param name="component">The component.</param>
		/// <returns>
		///   <c>true</c> if the specified component is active; otherwise, <c>false</c>.
		/// </returns>
		public static bool IsComponentActive(HudComponent component)
		{
			return NativeFunction.Natives.IS_HUD_COMPONENT_ACTIVE<bool>((int)component);
		}

		/// <summary>
		/// Queues the specified component to be shown for all frames in this tick.
		/// </summary>
		/// <param name="component">The component.</param>
		public static void ShowComponentThisFrame(HudComponent component)
		{
			NativeFunction.Natives.SHOW_HUD_COMPONENT_THIS_FRAME(component);
		}

		/// <summary>
		/// Instructs the game to hide the specified component for all frames in this tick unless another <see cref="ShowComponent(HudComponent)"/> 
		/// was called for such component again in this frame.
		/// </summary>
		/// <param name="component">The component.</param>
		public static void HideComponentThisFrame(HudComponent component)
		{
			NativeFunction.Natives.HIDE_HUD_COMPONENT_THIS_FRAME(component);
		}

		/// <summary>
		/// Queues the mouse cursor to be active in this frame.
		/// </summary>
		public static void ActivateCursorThisFrame()
		{
			NativeFunction.Natives._SET_MOUSE_CURSOR_ACTIVE_THIS_FRAME();
		}

		/// <summary>
		/// Gets or sets a value indicating whether any HUD components should be rendered.
		/// </summary>
		/// <value>
		/// <c>true</c> if components should be shown; otherwise, <c>false</c>.
		/// </value>
		public static bool IsVisible
		{
			get => !NativeFunction.Natives.IS_HUD_HIDDEN<bool>();
			set => NativeFunction.Natives.DISPLAY_HUD(value);
		}

		/// <summary>
		/// Gets or sets a value indicating whether the radar is visible.
		/// </summary>
		/// <value>
		/// <c>true</c> if radar is visible; otherwise, <c>false</c>.
		/// </value>
		public static bool IsRadarVisible
		{
			get => !NativeFunction.Natives.IS_RADAR_HIDDEN<bool>();
			set => NativeFunction.Natives.DISPLAY_RADAR(value);
		}

		/// <summary>
		/// Sets how far the mini-map should be zoomed in.
		/// </summary>
		/// <value>
		/// The radar zoom; accepts values from 0 to 200.
		/// </value>
#pragma warning disable S2376 // Write-only properties should not be used
		public static int RadarZoom
#pragma warning restore S2376 // Write-only properties should not be used
		{
			set => NativeFunction.Natives.SET_RADAR_ZOOM(value);
		}
	}
}
